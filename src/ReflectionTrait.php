<?php declare(strict_types=1);

namespace Warehouse;

trait ReflectionTrait
{
    protected function findValueForParameter(\ReflectionParameter $parameter)
    {
        $name = $parameter->getName();
        if ($this->has($name) === true) {
            return $this->get($name);
        }
        $parameterClass = $parameter->getClass();
        if ($parameterClass !== null) {
            if ($this->hasItemForClass($parameterClass->name) === true) {
                return $this->items[$this->classes[$parameterClass->name]];
            }
            if ($this->hasItemForInterface($parameterClass->name) === true) {
                return $this->items[$this->interfaces[$parameterClass->name]];
            }
        }
        return null;
    }

    protected function buildParameters(array $parameters) : array
    {
        return array_map([$this, 'findValueForParameter'], $parameters);
    }

    protected function determineParametersForClass(string $className) : array
    {
        $classToConstruct = new \ReflectionClass($className);
        return $this->buildParameters($classToConstruct->getConstructor()->getParameters());
    }

    protected function determineParametersForMethod($object, $methodName) : array
    {
        $reflectionMethod = new \ReflectionMethod($object, $methodName);
        return $this->buildParameters($reflectionMethod->getParameters());
    }

    protected function determineParametersForFunction(callable $function) : array
    {
        $reflectionFunction = new \ReflectionFunction($function);
        return $this->buildParameters($reflectionFunction->getParameters());
    }
}

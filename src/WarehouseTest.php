<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

class TestClass1
{
}

interface TestInterface1 {
    public function TestInterface1Method1();
    public function TestInterface1Method2();
}

class TestClass2 implements TestInterface1
{
    public function TestInterface1Method1()
    {

    }
    public function TestInterface1Method2()
    {

    }
}

class TestClass3
{
    public function methodToCall(\TestClass1 $objectToReturn)
    {
        return $objectToReturn;
    }
}

class TestClass4
{
    public $testParameter = null;
    public function __construct(\TestClass3 $testParameter)
    {
        $this->testParameter = $testParameter;
    }
}

final class WarehouseTest extends TestCase
{
    public function testCanBeInstantiated(): void
    {
        $this->assertInstanceOf(
            Warehouse\Warehouse::class,
            new Warehouse\Warehouse()
        );
    }

    public function testSimpleItemHas(): void
    {
        $goodItemId = 'testSimpleItemId1';
        $badItemId = 'testSimpleItemId2';
        $itemValue = 'test';
        $container = new Warehouse\Warehouse();
        $container->add($goodItemId, $itemValue);
        $this->assertTrue($container->has($goodItemId));
        $this->assertFalse($container->has($badItemId));        
    }

    public function testSimpleItemGet(): void
    {
        $itemId = 'testSimpleItemId';
        $itemValue = 'test';
        $container = new Warehouse\Warehouse();
        $container->add($itemId, $itemValue);
        $this->assertEquals($container->get($itemId), $itemValue);
    }

    public function testSimpleItemGetException(): void
    {
        $goodItemId = 'testSimpleItemId1';
        $badItemId = 'testSimpleItemId2';
        $itemValue = 'test';
        $container = new Warehouse\Warehouse();
        $container->add($goodItemId, $itemValue);
        $this->assertEquals($container->get($goodItemId), $itemValue);
        $this->expectException(\Psr\Container\NotFoundExceptionInterface::class);
        $container->get($badItemId);
    }

    public function testCallFunctionWithFoundInjectedArg(): void
    {
        $parameterId = 'testParameter';
        $parameterValue = 'testValue';
        $container = new Warehouse\Warehouse();
        $container->add($parameterId, $parameterValue);
        $testFunction = function(string $testParameter) {
            return $testParameter;
        };
        $result = $container->callFunction($testFunction);
        $this->assertEquals($result, $parameterValue);
    }

    public function testCallFunctionPassesNullsForNotFoundArgs(): void
    {
        $container = new Warehouse\Warehouse();
        $testFunction = function(string $testParameter = null) {
            return $testParameter;
        };
        $result = $container->callFunction($testFunction);
        $this->assertEquals($result, null);
    }

    public function testCallFunctionWithClassMatch(): void
    {
        $container = new Warehouse\Warehouse();
        $container->add('TestClass1', new TestClass1());
        $testFunction = function(TestClass1 $testParameter = null) {
            return $testParameter;
        };
        $result = $container->callFunction($testFunction);
        $this->assertEquals(get_class($result), TestClass1::class);
    }

    public function testCallFunctionWithInterfaceMatch(): void
    {
        $container = new Warehouse\Warehouse();
        $container->add('TestClass2', new TestClass2());
        $testFunction = function(TestInterface1 $testParameter = null) {
            return $testParameter;
        };
        $result = $container->callFunction($testFunction);
        $this->assertEquals(get_class($result), TestClass2::class);
    }

    public function testCallMethodWithClassMatch(): void
    {
        $container = new Warehouse\Warehouse();
        $container->add('TestClass1', new TestClass1());
        $testObject = new TestClass3();
        $result = $container->callMethod($testObject, 'methodToCall');
        $this->assertEquals(get_class($result), TestClass1::class);
    }

    public function testConstructWithParameterClassMatch(): void
    {
        $container = new Warehouse\Warehouse();
        $container->add('TestClass3', new TestClass3());
        $result = $container->construct(TestClass4::class);
        $this->assertEquals(get_class($result->testParameter), TestClass3::class);
    }
}

<?php declare(strict_types=1);

namespace Warehouse;

class NotFoundException extends \Exception  implements \Psr\Container\NotFoundExceptionInterface
{
}

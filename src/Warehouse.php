<?php declare(strict_types=1);

namespace Warehouse;

class Warehouse implements \Psr\Container\ContainerInterface
{
    use ReflectionTrait;
    protected $items = [];
    protected $classes = [];
    protected $interfaces = [];

    public function add(string $name, $item) : Warehouse
    {
        $this->items[$name] = $item;
        if (is_object($item) === true) {
            $this->classes[get_class($item)] = $name;
            $interfaces = class_implements(get_class($item));
            foreach ($interfaces as $interface) {
                $this->interfaces[$interface] = $name;
            }
        }
        return $this;
    }

    /**
     * Finds an entry of the container by its identifier and returns it.
     *
     * @param string $id Identifier of the entry to look for.
     *
     * @throws NotFoundExceptionInterface  No entry was found for **this** identifier.
     * @throws ContainerExceptionInterface Error while retrieving the entry.
     *
     * @return mixed Entry.
     */
    public function get($id)
    {
        if (isset($this->items[$id]) === false) {
            throw new NotFoundException();
        }
        return $this->items[$id];
    }

    /**
     * Returns true if the container can return an entry for the given identifier.
     * Returns false otherwise.
     *
     * `has($id)` returning true does not mean that `get($id)` will not throw an exception.
     * It does however mean that `get($id)` will not throw a `NotFoundExceptionInterface`.
     *
     * @param string $id Identifier of the entry to look for.
     *
     * @return bool
     */
    public function has($id)
    {
        return isset($this->items[$id]);
    }

    public function hasItemForClass(string $className)
    {
        return isset($this->classes[$className]);
    }

    public function hasItemForInterface(string $interfaceName)
    {
        return isset($this->interfaces[$interfaceName]);
    }

    public function construct(string $className)
    {
        return new $className(...$this->determineParametersForClass($className));
    }

    public function callMethod($object, string $methodName)
    {
        return $object->$methodName(...$this->determineParametersForMethod($object, $methodName));
    }

    public function callFunction(callable $functionToCall)
    {
        return $functionToCall(...$this->determineParametersForFunction($functionToCall));
    }
}

<?php declare(strict_types=1);

namespace Warehouse;

class ContainerException
    extends \Exception
    implements \Psr\Container\ContainerExceptionInterface
{
}